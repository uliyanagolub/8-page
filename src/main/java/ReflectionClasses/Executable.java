package ReflectionClasses;

public interface Executable {
    void execute();
}
