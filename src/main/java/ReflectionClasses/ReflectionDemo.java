package ReflectionClasses;

import AboutHuman.Human;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class ReflectionDemo{

    /*Дан список объектов произвольных типов. Найдите количество элементов списка, которые
    являются объектами типа Human или его подтипами.*/

    public static int getNumOfHumanObjects(List<Object> list){
        int sum = 0;
        for(Object element: list){
            if (element instanceof Human){
                sum++;
            }
        }
        return sum;
    }
    
    //Для объекта получить список имен его открытых методов. 
    
    public static List<String> getClassMethods(Object object){
        List<String> list = new ArrayList<>();
        for(Method e: object.getClass().getMethods()){
            list.add(e.getName());
        }
        return list;
    }
    
    /*Для объекта получить список (в виде списка строк) имен всех его суперклассов до класса
    Object включительно.*/
    
    public static List<String> namesOfClasses(Object object){
        List<String> list = new ArrayList<>();
        Class <?> someClass = object.getClass();
        while (someClass.getSuperclass() != null){
            someClass = someClass.getSuperclass();
            list.add(someClass.getName());
        }
        return list;
    }

    /*Напишите метод, который для списка объектов находит его элементы, реализующие этот
    интерфейс, и выполняет в таких объектах метод execute(). Метод возвращает количество
    найденных элементов.*/
    
    public static int numOfExecuteElements(List<Object> list) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        int res = 0;
        for (Object element: list){
            if (element instanceof Executable){
                Method method = element.getClass().getMethod("execute");
                res++;
        //((Executable) element).execute();
                method.invoke(element);
            }
        }
        return res;
    }

     // Для объекта получить список его геттеров и сеттеров
 
    public static List<String> getNamesOfGetters(Object object){
        List<String> result = new ArrayList<>();
        Method [] methods = object.getClass().getMethods();
        for (Method elem: methods){
            if (elem.getReturnType() != void.class && !Modifier.isStatic(elem.getModifiers())&&
                    Modifier.isPublic(elem.getModifiers())&& elem.getName().startsWith("get")&&
            elem.getParameterCount()==0){
                result.add(elem.getName());
            }
        }
        return result;
    }

    public static List<String> getNamesOfSetters(Object object){
        List<String> result = new ArrayList<>();
        Method [] methods = object.getClass().getMethods();
        for (Method elem: methods){
            if (elem.getReturnType() == void.class && !Modifier.isStatic(elem.getModifiers())&&
                    Modifier.isPublic(elem.getModifiers())&& elem.getName().startsWith("set")&&
                    elem.getParameterCount()==1){
                result.add(elem.getName());
            }
        }
        return result;
    }

}
