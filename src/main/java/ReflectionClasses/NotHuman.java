package ReflectionClasses;

public class NotHuman implements Executable{
    private String kind;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    private int population;

    public NotHuman(String kind, int population) {
        this.kind = kind;
        this.population = population;
    }

    @Override
    public void execute() {
        System.out.println("execute");
    }

}
