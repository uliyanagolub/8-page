package ReflectionClasses;

import AboutHuman.Student;

//подтип Human

public class StudentMathFaculty extends Student implements Executable{
    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    private String specialization;
    public StudentMathFaculty(String specialization, String surname, String name, String patronymic, int sge, String faculty) {
        super(surname, name, patronymic, sge, faculty);
        this.specialization = specialization;
    }

    @Override
    public void execute() {
        System.out.println("execute");
    }
}
