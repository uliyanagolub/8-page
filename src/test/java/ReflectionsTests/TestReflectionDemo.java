package ReflectionsTests;

import AboutHuman.Human;
import ReflectionClasses.NotHuman;
import ReflectionClasses.ReflectionDemo;
import ReflectionClasses.StudentMathFaculty;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class TestReflectionDemo {
    public Human human = new Human("Ivanov","Ivan", "Ivanovich", 20);
    public NotHuman notHuman = new NotHuman("Yeti", 1);
    public StudentMathFaculty studentMF= new StudentMathFaculty("programmer","Ivanov","Ivan",
            "Ivanovich", 20,"Math" );

    @Test
    public void testAmountOfHumanClassInList(){
        List <Object> objects = new ArrayList();
        objects.add(notHuman);
        objects.add(human);
        objects.add(3);
        objects.add(55.0);

        Assert.assertEquals(1, ReflectionDemo.getNumOfHumanObjects(objects));
    }

    @Test
    public void testGetMethodsForObject(){
        for (String s: ReflectionDemo.getClassMethods(notHuman)){
            System.out.println(s);
        }
    }

    @Test
    public void testGetAllSuperClasses() throws IOException {
        for (String s: ReflectionDemo.namesOfClasses(studentMF)){
            System.out.print(" -> " + s);
        }
    }

    @Test
    public void testGetAmountOfExecObjects() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        List<Object> list = new ArrayList<>();
        list.add(human);
        list.add(notHuman);
        list.add(studentMF);

        Assert.assertEquals(2, ReflectionDemo.numOfExecuteElements(list));
        Assert.assertEquals(0, ReflectionDemo.numOfExecuteElements(new ArrayList<>()));
    }

    @Test
    public void testGetSettersList(){
        for (String s: ReflectionDemo.getNamesOfSetters(notHuman)){
            System.out.println(s);
        }
    }

    @Test
    public void testGetGettersList(){
        for (String s: ReflectionDemo.getNamesOfGetters(notHuman)){
            System.out.println(s);
        }
    }

}

